import 'package:flutter/material.dart';

class MessagesView extends StatefulWidget
{
  @override
  State createState() => new MessagesViewState();
}

class MessagesViewState extends State<MessagesView>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new ListView
      (
        children: <Widget>
        [
          new MessageCardUI(new Message("Ivascu Adrian", "My first PC Gaming", 25, "Hi Maxi PC. I heard you are building computers. I would need a PC so I can play some games. My budget is of 300\$. I already have a monitor, a mouse and a keyboard. Thank you."))
        ],
      )
    );
  }
}

class MessageCardUI extends StatelessWidget
{
  final Message message;
  MessageCardUI(this.message);

  @override
  Widget build(BuildContext context)
  {
    return new Card
    (
      child: new InkWell
      (
        onTap: () => null,
        child: new ListTile
        (
          title: new Text("[RE] ${message.subject}"),
          subtitle: new Text("From: ${message.from}"),
          trailing: new Text("25/07/2002"),
        ),
      ),
    );
  }
}

class Message
{
  String from;
  String subject;
  int date;
  String message;

  Message(this.from, this.subject, this.date, this.message);
}