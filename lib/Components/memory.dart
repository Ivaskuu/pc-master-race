import 'component.dart';

class Memory extends Component
{
  int speed;
  int capacity;
  int powerConsumption;

  Memory(String brand, String model, String image, int releaseDate, double price, this.speed, this.capacity, this.powerConsumption)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${capacity} Gb, ${speed} Mb/s, ${powerConsumption} W";
  }
}