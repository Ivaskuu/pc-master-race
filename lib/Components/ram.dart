import 'component.dart';

class RAM extends Component
{
  int capacity; // In MB
  double speed;
  int powerConsumption;
  double overclockRatio;

  RAM(String brand, String model, String image, int releaseDate, double price, this.capacity, this.speed, this.powerConsumption, this.overclockRatio)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${capacity} MB, ${speed} GHz, ${powerConsumption} W";
  }
}