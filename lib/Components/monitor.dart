import 'component.dart';

class Monitor extends Component
{
  int refreshRate;
  int resolution;

  Monitor(String brand, String model, String image, int releaseDate, double price, this.refreshRate, this.resolution)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${refreshRate} Hz, ${resolution}p";
  }
}