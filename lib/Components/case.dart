import 'component.dart';

enum FormFactor
{
  microATX, miniATX, ATX
}

class Case extends Component
{
  FormFactor formFactor;
  int airflow; // from 0 to 100

  Case(String brand, String model, String image, int releaseDate, double price, this.formFactor, this.airflow)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${formFactor.toString().split('.')[1]}";
  }
}