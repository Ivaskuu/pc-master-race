import 'component.dart';

enum MouseType
{
  ball, optical, wireless, gaming
}

class Mouse extends Component
{
  double speed;
  MouseType mouseType;

  Mouse(String brand, String model, String image, int releaseDate, double price, this.speed, this.mouseType)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${mouseType.toString().split('.')[1]}, ${speed} Hz";
  }
}