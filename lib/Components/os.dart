import 'component.dart';

class OS extends Component
{
  int popularity;

  OS(String brand, String model, String image, int releaseDate, double price, this.popularity)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${popularity}% popularity";
  }
}