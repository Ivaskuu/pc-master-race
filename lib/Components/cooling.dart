import 'component.dart';

class CoolingSystem extends Component
{
  int powerConsumption;
  int coolingAmount;

  CoolingSystem(String brand, String model, String image, int releaseDate, double price, this.powerConsumption, this.coolingAmount)
   : super(brand, model, image, releaseDate, price);
}