import 'component.dart';

class GPU extends Component
{
  double speed;
  int cores;
  int vram;
  double operatingTemperature;
  int powerConsumption;
  double overclockRatio;

  GPU(String brand, String model, String image, int releaseDate, double price, this.speed, this.cores, this.vram, this.operatingTemperature, this.powerConsumption, this.overclockRatio)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${speed} GHz, ${cores} cores, ${vram} VRAM, ${powerConsumption} W";
  }
}