import 'component.dart';

enum KeyboardType
{
  base, wireless, gaming
}

class Keyboard extends Component
{
  KeyboardType keyboardType;
  int comfortability;

  Keyboard(String brand, String model, String image, int releaseDate, double price, this.keyboardType, this.comfortability)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${keyboardType.toString().split('.')[1]}, ${comfortability} comfortability";
  }
}