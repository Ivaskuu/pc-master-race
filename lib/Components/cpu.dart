import 'component.dart';

class CPU extends Component
{
  double speed;
  int cores;
  String socket;
  double operatingTemperature;
  int powerConsumption;
  double overclockRatio;

  CPU(String brand, String model, String image, int releaseDate, double price, this.speed, this.cores, this.socket, this.operatingTemperature, this.powerConsumption, this.overclockRatio)
   : super(brand, model, image, releaseDate, price);
  
  @override
  String getImportantAttributesString()
  {
    return "${speed} GHz, ${cores} cores, ${socket} socket, ${powerConsumption} W";
  }
}