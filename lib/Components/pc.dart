import 'case.dart';
import 'motherboard.dart';
import 'cpu.dart';
import 'gpu.dart';
import 'ram.dart';
import 'memory.dart';
import 'cooling.dart';
import 'psu.dart';
import 'monitor.dart';
import 'mouse.dart';
import 'keyboard.dart';
import 'os.dart';

class PC
{
  Case pcCase;
  Motherboard motherboard;

  CPU cpu;
  List<GPU> gpus = new List();
  List<RAM> rams = new List();
  Memory memory;

  CoolingSystem coolingSystem;
  PSU psu;

  List<Monitor> monitors = new List();
  Mouse mouse;
  Keyboard keyboard;
  
  OS os;
}