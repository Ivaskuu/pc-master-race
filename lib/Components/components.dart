/*
  Stores all the game components
*/

import 'case.dart';
import 'motherboard.dart';
import 'cpu.dart';
import 'gpu.dart';
import 'ram.dart';
import 'memory.dart';
import 'cooling.dart';
import 'psu.dart';
import 'monitor.dart';
import 'mouse.dart';
import 'keyboard.dart';
import 'os.dart';

import '../inventory.dart' as inventory;

class Components
{
  static List<Case> cases = new List();
  static List<Motherboard> motherboards = new List();
  static List<CPU> cpus = new List();
  static List<GPU> gpus = new List();
  static List<RAM> rams = new List();
  static List<Memory> memories = new List();
  static List<CoolingSystem> coolingSystems = new List();
  static List<PSU> psus = new List();
  static List<Monitor> monitors = new List();
  static List<Mouse> mouses = new List();
  static List<Keyboard> keyboards = new List();
  static List<OS> operatingSystems = new List();

  static void loadComponents() // Ordered by the release date
  {
    cases.add(new Case("Chainize", "MikroKase", "images/cpu.png", 0, 20.0, FormFactor.microATX, 10));
    cases.add(new Case("Hoter Master", "Mini 200", "images/cpu.png", 0, 50.0, FormFactor.miniATX, 30));
    motherboards.add(new Motherboard("Assrock", "MyBoard Micro 100", "images/cpu.png", 0, 30.0, "L120", FormFactor.microATX, 1, 1));
    motherboards.add(new Motherboard("Kilobit", "H100H", "images/cpu.png", 0, 40.0, "RSocket 3", FormFactor.microATX, 1, 1));
    motherboards.add(new Motherboard("Kilobit", "H110M", "images/cpu.png", 0, 70.0, "RSocket 3", FormFactor.miniATX, 2, 1));
    cpus.add(new CPU("Intelligent", "i7 3960X", "images/cpu.png", 0, 50.0, 1.1, 1, "L120", 60.0, 30, 0.0));
    cpus.add(new CPU("Intelligent", "iCore 2", "images/cpu.png", 0, 100.0, 1.2, 2, "L120", 65.0, 50, 0.0));
    cpus.add(new CPU("AND", "GX 315", "images/cpu.png", 0, 75.0, 1.4, 2, "RSocket 3", 70.0, 70, 0.0));
    gpus.add(new GPU("ADI", "Raedon 9000", "images/cpu.png", 0, 150.0, 0.2, 24, 128, 80.0, 120, 0.0));
    rams.add(new RAM("Crossair", "DDR 256", "images/cpu.png", 0, 20.0, 256, 0.2, 3, 0.0));
    rams.add(new RAM("Crossair", "DDR 512", "images/cpu.png", 0, 40.0, 512, 0.2, 4, 0.0));
    rams.add(new RAM("Important", "XDDR 256", "images/cpu.png", 0, 40.0, 256, 0.3, 5, 0.0));
    memories.add(new Memory("Eastern Digital", "Blue 32 5400", "images/cpu.png", 0, 20.0, 100, 32, 5));
    memories.add(new Memory("Eastern Digital", "Red 32 7200", "images/cpu.png", 0, 50.0, 300, 64, 5));
    psus.add(new PSU("Volt", "100W", "images/cpu.png", 0, 35.0, 100));
    psus.add(new PSU("Volt", "200W", "images/cpu.png", 0, 70.0, 200));
    monitors.add(new Monitor("See", "Blurred", "images/cpu.png", 0, 50.0, 20, 240));
    mouses.add(new Mouse("Maus", "BallOne", "images/cpu.png", 0, 10.0, 0.1, MouseType.ball));
    mouses.add(new Mouse("Maus", "Optikal", "images/cpu.png", 0, 100.0, 0.3, MouseType.optical));
    keyboards.add(new Keyboard("Illogical Tech", "Keys", "images/cpu.png", 0, 15.0, KeyboardType.base, 15));
    operatingSystems.add(new OS("Doors", "xD", "images/cpu.png", 0, 50.0, 80));
    operatingSystems.add(new OS("North Pole", "Penguin", "images/cpu.png", 0, 10.0, 5));

    inventory.Inventory.components.addAll(cases);
    inventory.Inventory.components.addAll(motherboards);
    inventory.Inventory.components.addAll(cpus);
    inventory.Inventory.components.addAll(gpus);
    inventory.Inventory.components.addAll(rams);
    inventory.Inventory.components.addAll(memories);
    inventory.Inventory.components.addAll(psus);
    inventory.Inventory.components.addAll(monitors);
    inventory.Inventory.components.addAll(mouses);
    inventory.Inventory.components.addAll(keyboards);
    inventory.Inventory.components.addAll(operatingSystems);
  }
}