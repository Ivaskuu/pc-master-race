import 'component.dart';
import 'case.dart';

class Motherboard extends Component
{
  FormFactor formFactor;
  
  String chipset;
  int ramSlots;
  int gpuSlots;

  Motherboard(String brand, String model, String image, int releaseDate, double price, this.chipset, this.formFactor, this.ramSlots, this.gpuSlots)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${formFactor.toString().split('.')[1]}, ${chipset} chipset";
  }
}