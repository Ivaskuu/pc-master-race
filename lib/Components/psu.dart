import 'component.dart';

class PSU extends Component
{
  int power;

  PSU(String brand, String model, String image, int releaseDate, double price, this.power)
   : super(brand, model, image, releaseDate, price);

  @override
  String getImportantAttributesString()
  {
    return "${power} W";
  }
}