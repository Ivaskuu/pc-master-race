import 'package:flutter/material.dart';
import 'main.dart' as main;
import 'Assemble/assemble_view.dart';
import 'Shop/shop_view.dart';
import 'Messages/messages_view.dart';

class NavigationDrawerView extends StatefulWidget
{
  @override
  State createState() => new NavigationDrawerViewState();
}

class NavigationDrawerViewState extends State<NavigationDrawerView>
{
  @override
  Widget build(BuildContext context)
  {
    return new Drawer
    (
      child: new ListView
      (
        children: <Widget>
        [
          new Container
          (
            margin: new EdgeInsets.only(top: 52.0, bottom: 32.0),
            child: new Column
            (
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new Container
                (
                  margin: new EdgeInsets.only(bottom: 16.0),
                  child: new CircleAvatar(radius: 40.0, backgroundColor: Colors.amber, child: new FlutterLogo(size: 50.0))
                ),
                new Text("Maxi-PC", style: Theme.of(context).textTheme.headline),
                new Container
                (
                  margin: new EdgeInsets.only(bottom: 16.0),
                  child: new Text("Reputation: Unknown"),
                ),
                new Chip(label: new Text("317", style: Theme.of(context).textTheme.title), avatar: new Icon(Icons.euro_symbol))
              ],
            ),
          ),
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              main.mainView.changeActualView(new AssembleView(), "Assemble View");
            },
            child: new ListTile
            (
              leading: new Icon(Icons.build),
              title: new Text("Assemble")
            ),
          ),
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              main.mainView.changeActualView(new MessagesView(), "Messages View");
            },
            child: new ListTile
            (
              leading: new Icon(Icons.message),
              title: new Row
              (
                children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 16.0), child: new Text("Messages")),
                  new CircleAvatar(backgroundColor: Colors.red, radius: 10.0, child: new Text("3", style: new TextStyle(fontSize: 15.0))),
                ],
              ),
            ),
          ),
          new InkWell
          (
            onTap: ()
            {
              Navigator.pop(context);
              main.mainView.changeActualView(new ShopView(), "Shop View");
            },
            child: new ListTile
            (
              leading: new Icon(Icons.shopping_cart),
              title: new Text("Shop")
            ),
          ),
          new InkWell
          (
            onTap: () => Navigator.pop(context),
            child: new ListTile
            (
              leading: new Icon(Icons.info),
              title: new Text("Infos")
            ),
          ),
        ],
      )
    );
  }
}