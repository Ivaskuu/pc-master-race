import 'package:flutter/material.dart';
import 'navigation_drawer_view.dart';
import 'Assemble/assemble_view.dart';
import 'Components/components.dart' as componentsClass;

void main()
{
  componentsClass.Components.loadComponents();
  runApp(new MyApp());
}

MainViewState mainView;

Widget _actualView = new AssembleView();
String _appBarTitle = "Assemble View";

class MyApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new MaterialApp
    (
      title: "PC Master Race",
      theme: new ThemeData
      (
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MainView(),
      /*routes: <String, WidgetBuilder>
      {
        "/Assemble": (BuildContext context) => new AssembleView(),
        "/Shop": (BuildContext context) => new ShopView()
      },*/
    );
  }
}

class MainView extends StatefulWidget
{
  @override
  State createState() => new MainViewState();
}

class MainViewState extends State<MainView>
{
  @override
  Widget build(BuildContext context)
  {
    mainView = this;

    return new Scaffold
    (
      appBar: new AppBar
      (
        title: new Text(_appBarTitle),
        actions: [new IconButton(icon: new Icon(Icons.update), onPressed: () => newDay())],
      ),
      body: _actualView,
      drawer: new NavigationDrawerView(),
    );
  }

  void newDay()
  {
    showDialog
    (
      context: context,
      child: new SimpleDialog
      (
        children: <Widget>
        [
          new Column
          (
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new CircleAvatar(radius: 40.0),
              new Container(margin: new EdgeInsets.only(top: 16.0), child: new Text("25/09/2002", style: Theme.of(context).textTheme.headline)),
              new Container(margin: new EdgeInsets.only(top: 16.0), child: new Text("No deliveries", style: Theme.of(context).textTheme.body1)),
              new Container(margin: new EdgeInsets.only(top: 16.0), child: new Text("No new technology", style: Theme.of(context).textTheme.body2))
            ],
          )
        ],
      )
    );
  }

  void changeActualView(Widget view, String title)
  {
    setState(()
    {
      _actualView = view;
      _appBarTitle = title;
    });
  }
}