import 'package:flutter/material.dart';
import '../Components/component.dart';
import '../inventory.dart' as inventory;

class InventoryView extends StatefulWidget
{
  final String category;
  InventoryView(this.category);

  @override
  State createState() => new InventoryViewState();
}

class InventoryViewState extends State<InventoryView>
{
  final List<Component> matchingComponents = new List();
  final List<Widget> inventoryViewTiles = new List();

  @override
  void initState()
  {
    for(int i = 0; i < inventory.Inventory.components.length; i++)
    {
      if(inventory.Inventory.components[i].runtimeType.toString() == widget.category)
      {
        matchingComponents.add(inventory.Inventory.components[i]);
        inventoryViewTiles.add(new InventoryViewTile(inventory.Inventory.components[i]));
      }
    }

    inventoryViewTiles.insert(0, new Card
    (
      child: new InkWell
      (
        onTap: () => Navigator.pop(context, "Nothing"),
        child: new ListTile
        (
          leading: new Icon(Icons.not_interested),
          subtitle: new Text("Nothing"),
        ),
      )
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    if(matchingComponents.length > 0)
    {
      return new Scaffold
      (
        appBar: new AppBar(title: new Text("Inventory")),
        body: new Column
        (
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>
          [
            new Container
            (
              child: new ListTile
              (
                leading: new Icon(Icons.smartphone),
                title: new Text(widget.category, style: Theme.of(context).textTheme.title, textScaleFactor: 1.5)
              )
            ),
            new Expanded
            (
              child: new ListView.builder
              (
                itemBuilder: (_, int index) => inventoryViewTiles[index],
                itemCount: inventoryViewTiles.length,
              ),
            )
          ]
        )
      );
    }
    else
    {
      return new Scaffold
      (
        appBar: new AppBar(title: new Text("Inventory")),
        body: new Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.only(bottom: 16.0),
              child: new Text("Uh Oh..", textAlign: TextAlign.center, style: Theme.of(context).textTheme.title, textScaleFactor: 1.5),
            ),
            new Container
            (
              margin: new EdgeInsets.only(left: 64.0, right: 64.0, bottom: 64.0),
              child: new Text("Uh Oh.. You don't have any of the component you're looking for", textAlign: TextAlign.center, style: Theme.of(context).textTheme.body1),
            ),
            new Container
            (
              margin: new EdgeInsets.all(16.0),
              child: new RaisedButton
              (
                onPressed: () => Navigator.pop(context),
                color: Colors.black,
                child: new Text("GO TO THE SHOP", style: new TextStyle(color: Colors.white)),
              )
            )
          ],
        ),
      );
    }
  }
}

class InventoryViewTile extends StatelessWidget
{
  final Component component;
  InventoryViewTile(this.component);

  @override
  Widget build(BuildContext context)
  {
    return new Card
    (
      child: new InkWell
      (
        onTap: () => Navigator.pop(context, component),
        onLongPress: () => showDetails(context),
        child: new Column
        (
          children: <Widget>
          [
            new ListTile
            (
              leading: new Image.asset(component.image),
              title: new Text("${component.brand} - ${component.model}"),
              subtitle: new Text(component.getImportantAttributesString()),
              trailing: new Text("3")
            ),
          ],
        )
      ),
    );
  }

  void showDetails(BuildContext context)
  {
    showDialog
    (
      context: context,
      child: new SimpleDialog
      (
        children: <Widget>
        [
          new Container
          (
            margin: new EdgeInsets.only(bottom: 16.0),
            child: new Row
            (
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new Image.asset("images/cpu.png", width: 100.0),
                new Column
                (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>
                  [
                    new Container
                    (
                      margin: new EdgeInsets.only(bottom: 2.0),
                      child: new Text(component.model, style: Theme.of(context).textTheme.title)
                    ),
                    new Container
                    (
                      margin: new EdgeInsets.only(bottom: 12.0),
                      child: new Text(component.brand, style: Theme.of(context).textTheme.subhead),
                    ),
                    new RaisedButton
                    (
                      onPressed: () { Navigator.pop(context); Navigator.pop(context, component); },
                      child: new Text("ADD", style: new TextStyle(color: Colors.white)),
                      color: Colors.blueAccent
                    )
                  ],
                )
              ],
            ),
          ),
          new Container
          (
            margin: new EdgeInsets.only(bottom: 16.0),
            child: new Row
            (
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>
              [
                new Column
                (
                  children: <Widget>
                  [
                    new Text("Speed", style: Theme.of(context).textTheme.caption),
                    new Text("3.4 GHz", style: Theme.of(context).textTheme.title),
                  ],
                ),
                new Column
                (
                  children: <Widget>
                  [
                    new Text("Cores", style: Theme.of(context).textTheme.caption),
                    new Text("2", style: Theme.of(context).textTheme.title),
                  ],
                ),
                new Column
                (
                  children: <Widget>
                  [
                    new Text("Temp", style: Theme.of(context).textTheme.caption),
                    new Text("80°C", style: Theme.of(context).textTheme.title),
                  ],
                ),
              ],
            ),
          ),
          new Row
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>
            [
              new Column
              (
                children: <Widget>
                [
                  new Text("Power", style: Theme.of(context).textTheme.caption),
                  new Text("60 W", style: Theme.of(context).textTheme.title),
                ],
              ),
              new Column
              (
                children: <Widget>
                [
                  new Text("Overclock", style: Theme.of(context).textTheme.caption),
                  new Text("0%", style: Theme.of(context).textTheme.title),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}