import 'package:flutter/material.dart';
import 'inventory_view.dart';
import '../Components/pc.dart';
import '../Components/component.dart';

class AssembleView extends StatefulWidget
{
  final PC pc1 = new PC();

  @override
  State createState() => new AssembleViewState();
}

class AssembleViewState extends State<AssembleView>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new Container
      (
        child: new Stack
        (
          children: <Widget>
          [
            new Column
            (
              children: <Widget>
              [
                new Image.asset("images/pc.jpg", height: 250.0),
                new Expanded
                (
                  child: new ListView
                  (
                    children: <Widget>
                    [
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Case"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.pcCase = null);
                            else if(component != null) setState(() => widget.pc1.pcCase = component);
                          }),
                          child: categoryListTile(widget.pc1.pcCase, "Case", Icons.smartphone, false)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.pcCase != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Motherboard"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.motherboard = null);
                            else if(component != null) setState(() => widget.pc1.motherboard = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.motherboard, "Motherboard", Icons.dashboard, true, requirement: widget.pc1.pcCase)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("CPU"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.cpu = null);
                            else if(component != null) setState(() => widget.pc1.cpu = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.cpu, "CPU", Icons.memory, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("GPU"))).then
                          ((var component)
                          {
                            //if(component != null && component == "Nothing") setState(() => widget.pc1.gpus[0] = null);
                            //else if(component != null) setState(() => widget.pc1.gpus[0] = component);
                          }) : null,
                          child: categoryListTile(/*widget.pc1.gpus[0]*/ null, "GPU", Icons.developer_board, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("RAM"))).then
                          ((var component)
                          {
                            //if(component != null && component == "Nothing") setState(() => widget.pc1.rams[0] = null);
                            //else if(component != null) setState(() => widget.pc1.rams[0] = component);
                          }) : null,
                          child: categoryListTile(/*widget.pc1.rams[0]*/ null, "RAM", Icons.credit_card, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Memory"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.memory= null);
                            else if(component != null) setState(() => widget.pc1.memory = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.memory, "Memory", Icons.storage, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("PSU"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.psu = null);
                            else if(component != null) setState(() => widget.pc1.psu = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.psu, "PSU", Icons.power, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.motherboard != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("CoolingSystem"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.coolingSystem = null);
                            else if(component != null) setState(() => widget.pc1.coolingSystem = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.coolingSystem, "CoolingSystem", Icons.toys, true, requirement: widget.pc1.motherboard)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Monitor"))).then
                          ((var component)
                          {
                            //if(component != null && component == "Nothing") setState(() => widget.pc1.gpus[0] = null);
                            //else if(component != null) setState(() => widget.pc1.gpus[0] = component);
                          }),
                          child: categoryListTile(/*widget.pc1.gpus[0]*/ null, "Monitor", Icons.desktop_windows, false)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Mouse"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.mouse = null);
                            else if(component != null) setState(() => widget.pc1.mouse = component);
                          }),
                          child: categoryListTile(widget.pc1.mouse, "Mouse", Icons.mouse, false)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("Keyboard"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.keyboard = null);
                            else if(component != null) setState(() => widget.pc1.keyboard = component);
                          }),
                          child: categoryListTile(widget.pc1.keyboard, "Keyboard", Icons.dashboard, false)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: widget.pc1.memory != null ? () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InventoryView("OS"))).then
                          ((var component)
                          {
                            if(component != null && component == "Nothing") setState(() => widget.pc1.os = null);
                            else if(component != null) setState(() => widget.pc1.os = component);
                          }) : null,
                          child: categoryListTile(widget.pc1.os, "OS", Icons.security, true, requirement: widget.pc1.memory)
                        )
                      ),
                      new Card
                      (
                        child: new InkWell
                        (
                          onTap: () => null,
                          child: new ListTile
                          (
                            leading: new Icon(Icons.more_horiz),
                            title: new Text("Optionals"),
                            subtitle: new Text("Nothing selected"),
                            trailing: new Icon(Icons.arrow_forward),
                          )
                        )
                      ),
                    ],
                  )
                ),
              ],
            ),
            new Align
            (
              alignment: new FractionalOffset(0.93, 0.435),
              child: new FloatingActionButton
              (
                onPressed: () => print(widget.pc1.pcCase),
                child: new Icon(Icons.build),
                backgroundColor: Colors.black54,
                elevation: 0.0
              ),
            ),
          ],
        )
      ),
    );
  }

  Widget categoryListTile(Component component, String categoryName, IconData icon, bool requiresComponentFirst, {Component requirement})
  {
    if(requiresComponentFirst && requirement == null) // If it requires a required component first but the component is null
    {
      return new ListTile
      (
        leading: new Icon(icon),
        subtitle: new Text("Add a motherboard first"),
      );
    }
    else if(component != null) // Else if it doesn't require a component or the required component is not null, and the player has already added a component
    {
      return new ListTile
      (
        leading: new Icon(icon),
        title: new Text(categoryName),
        subtitle: new Text("${component.brand} ${component.model}"),
        trailing: new Container(margin: new EdgeInsets.symmetric(vertical: 4.0), child: new Image.asset(component.image)),
      );
    }
    else // Else if it doesn't require a component or the required component is not null, and the player hasn't already added a component
    {
      return new ListTile
      (
        leading: new Icon(icon),
        title: new Text(categoryName),
        subtitle: new Text("Select a component..."),
        trailing: new Icon(Icons.arrow_forward)
      );
    }
  }
}