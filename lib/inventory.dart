/*
  Stores the components the player has bought, but not yet assigned to a PC.
*/

import 'Components/component.dart';

class Inventory
{
  static List<Component> components = new List(); /* The list of components the inventory has */
  int inventorySize; /* The maximum number of components the inventory can contain */

  static void printCiao()
  {
    print("Ciao");
  }
}